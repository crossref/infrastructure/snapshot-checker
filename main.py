"""
Check the status of the previous months XML/JSON snapshots. We expect that both the
XML and JSON files were created, stored in the snapshots bucket and accessible via the
cayenne REST API. We send a notification to let us know the status of the snapshots
so that we know if either of the files arent there.
"""

import os
from datetime import datetime, timedelta

import boto3
import requests
from bs4 import BeautifulSoup, SoupStrainer

API_DOMAIN = os.environ.get("API_DOMAIN")
SNAPSHOTS_BUCKET = os.environ.get("SNAPSHOTS_BUCKET")


def get_date():
    """
    Get the date for the previous month in the format YYYY and MM
    """
    today = datetime.now()
    first = today.replace(day=1)
    previous_month = first - timedelta(days=1)
    year = previous_month.strftime("%Y")
    month = previous_month.strftime("%m")
    return year, month

def check_api():
    """
    Check the cayenne API for available snapshots

    This gets the HTML from the response, grabs all the links and returns the list of files

    Page example:

    ```
    <h1>/monthly/2023/04</h1><ol><li><a href='/snapshots/monthly/2023/04/all.json.tar.gz'>all.json.tar.gz</a></li><li><a href='/snapshots/monthly/2023/04/all.xml.tar.gz'>all.xml.tar.gz</a></li></ol>
    ```
    """
    year, month = get_date()
    files_list = []
    url = f"https://{API_DOMAIN}/snapshots/monthly/{year}/{month}?mailto=snapshot-checker@crossref.org"

    r = requests.get(url, timeout=10)

    for link in BeautifulSoup(r.text, parse_only=SoupStrainer("a"), features="lxml"):
        if link.has_attr("href"):
            files_list.append(link["href"])

    return files_list

def check_bucket():
    """
    Check the snapshots bucket for available snapshots
    """
    year, month = get_date()
    s3 = boto3.resource("s3")
    snapshots_bucket = s3.Bucket(SNAPSHOTS_BUCKET)
    files_list = []

    try:
        for bucket_file in snapshots_bucket.objects.filter(Prefix=f"monthly/{year}/{month}"):
            files_list.append(bucket_file.key)
    except:
        pass

    return files_list

def check_results():
    """
    Compare the returned files lists from checking the API and bucket with what we expect to be returned
    """

    year, month = get_date()
    api_files_list = check_api()
    api_expected_results = [
        f"/snapshots/monthly/{year}/{month}/all.json.tar.gz",
        f"/snapshots/monthly/{year}/{month}/all.xml.tar.gz"
    ]

    bucket_files_list = check_bucket()
    bucket_expected_results = [
        f"monthly/{year}/{month}/all.json.tar.gz",
        f"monthly/{year}/{month}/all.xml.tar.gz"
    ]


    if sorted(api_files_list) == sorted(api_expected_results) and sorted(bucket_files_list) == sorted(bucket_expected_results):
        print(f"The XML and JSON files for {year}-{month} appear to be correct")
        print(f"API shows: {api_files_list}")
        print(f"Bucket shows: {bucket_files_list}")
    else:
        print("Found a problem with the snapshot generation, the file(s) arent there as expected.")
        print(f"API shows: {api_files_list}")
        print(f"Expected: {api_expected_results}")
        print(f"Bucket shows: {bucket_files_list}")
        print(f"Expected: {bucket_expected_results}")

check_results()
