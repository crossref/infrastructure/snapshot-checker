# snapshot-checker

Checks if the XML and JSON snapshot files were created as expected. Sends a notification if there are any problems detected.

- Check that cayenne is showing both files on `https://api.crossref.org/snapshots/monthly/$year/$month` for the previous month
- Connect to snapshots bucket and verify that this months files for both XML and JSON exist
- Send notification with results

## Settings

Requires environment variables for:

- API_DOMAIN - Set to the domain name only for the cayenne API
- SNAPSHOTS_BUCKET - The bucket name where snapshots are stored


## Logging

It should print into stdout the status of the snapshots check.

On AWS this is captured by CloudWatch. The invocation of the task via EventBridge event cron schedule will not log directly. Instead you need to go into CloudTrail, Event history and look at the `RunTask` results.
